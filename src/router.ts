import {createWebHashHistory, createRouter} from 'vue-router';
import {h} from 'vue';
import Home from './views/Home.vue';
import Doc from './views/Doc.vue';
import ButtonDemo from './components/ButtonDemo.vue';
import SwitchDemo from './components/SwitchDemo.vue';
import DialogDemo from './components/DialogDemo.vue';
import TabDemo from './components/TabDemo.vue';
import BadgeDemo from './components/BadgeDemo.vue';
import Markdown from './components/Markdown.vue';
import intro from './markdown/intro.md';
import install from './markdown/install.md';
import use from './markdown/use.md';

const markdown = string => h(Markdown, {content: string, key: string});

const history = createWebHashHistory();
const router = createRouter({
    history,
    routes: [
        {path: '/', component: Home},
        {
            path: '/doc',
            component: Doc,
            children: [
                {path: 'intro', component: markdown(intro)},
                {path: 'install', component: markdown(install)},
                {path: 'use', component: markdown(use)},
                {path: 'button', component: ButtonDemo},
                {path: 'switch', component: SwitchDemo},
                {path: 'dialog', component: DialogDemo},
                {path: 'tab', component: TabDemo},
                {path: 'badge', component: BadgeDemo},
            ]
        },
    ]
});

export {router};